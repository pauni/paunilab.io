+++
title = "RAM Over Ethernet (RÖ)"
date = "2019-10-12T12:00:00-00:00"
+++

ROE or RÖ is a technology that allows your custumers to download more RAM as they need it.
A fine crafted Protocol allows latencies you've never seen before. State of the art cryptography makes sure,
that your RAM is protected against malicious attacks such as Buffer Overflows and Memory leaks.

<!--more-->

## Want it?
Write us an email to get a pricing. We can deliver up to 4Tb RAM.
Price do not include shipping.

## Are you ready for the next big thing?
